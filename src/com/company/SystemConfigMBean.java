package com.company;

public interface SystemConfigMBean {

    public void setThreadCount(int count);
    public int getThreadCount();

    public void setMapSize(int size);
    public int getMapSize();

    public String displayInfo();
}