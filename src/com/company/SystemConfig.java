package com.company;

public class SystemConfig implements SystemConfigMBean {

    private int threadsCount;
    private int mapSize;
    public int mapRecords = 0;
    public double ratio = 0;

    public SystemConfig(int threadsCount, int mapSize){
        this.threadsCount = threadsCount;
        this.mapSize = mapSize;
    }

    @Override
    public void setThreadCount(int count) {
        this.threadsCount = count;
    }


    @Override
    public int getThreadCount() {
        return this.threadsCount;
    }


    @Override
    public void setMapSize(int size) {
        this.mapSize = size;
    }


    @Override
    public int getMapSize() {
        return this.mapSize;
    }

    @Override
    public String displayInfo() {
        return "Wątki: " + getThreadCount() + " | Rozmiar " + mapRecords + "/" + this.getMapSize() + " " + ratio + "%";
    }
}
