package com.company;

import java.util.*;

public class SolutionMap<Long, SolutionMapElement> extends AbstractMap<Long, SolutionMapElement> {
    private Map<Long, SolutionMapElement> solutions;

    /** Liczba odwołań */
    public Integer appeals = 0;

    /** Liczba odwołań nieudanych (klucz nieistnieje)*/
    public Integer failedAppeals = 0;

    private Integer size = 10000;

    public SolutionMap(int size) {
        solutions = new HashMap<Long, SolutionMapElement>();
        this.size = size;
    }

    @Override
    public Set<Entry<Long, SolutionMapElement>> entrySet() {
        throw new UnsupportedOperationException();
    }

    public synchronized SolutionMapElement get(Object key) {
        SolutionMapElement o = solutions.get(key);
        if (o == null) {
            return null;
        }
        return o;
    }

    public synchronized SolutionMapElement put(Long key, SolutionMapElement value) {
        if(size() >= size) {
            Iterator<Long> it = solutions.keySet().iterator();
            this.remove(it.next());
        }

        SolutionMapElement old = solutions.put(key, value);
        return old == null ? null : old;
    }

    public synchronized SolutionMapElement remove(Object key) {
        SolutionMapElement ref = solutions.remove(key);
        return ref == null ? null : ref;
    }

    public synchronized void clear() {
        solutions.clear();
    }

    @Override
    public synchronized boolean containsKey(Object key) {
        appeals++;

        if (!solutions.containsKey(key)) {
            failedAppeals++;
            return false;
        }

        if (solutions.get(key) == null) {
            return false;
        }

        return true;
    }

    public Float ratio() {
        if(appeals == 0) {
            return (float) 0;
        }

        return (float) failedAppeals / (float) appeals * 100;
    }

    public synchronized int size() {
        return solutions.size();
    }

    public synchronized void resetAppeals() {
        appeals = 0;
        failedAppeals = 0;
    }

    public synchronized void changeMapSize(int size) {
        if (this.size() > size) {
            Set<Long> keys = solutions.keySet();

            int itemsToDelete = keys.size() - size;

            System.out.println(size + " " + size() + " " + itemsToDelete);

            if (itemsToDelete > 0) {
                int i = 0;

                Iterator<Long> iter = keys.iterator();

                while (iter.hasNext()) {
                    Long key = iter.next();

                    if (i < itemsToDelete) {
                        iter.remove();
                    } else {
                        break;
                    }

                    i++;
                }
            }
        }

        this.size = size;
    }
}
