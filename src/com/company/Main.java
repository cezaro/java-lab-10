package com.company;

import javax.management.*;
import java.io.File;
import java.lang.management.ManagementFactory;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.text.DecimalFormat;
import java.util.*;

public class Main {
    private static final int THREADS_COUNT = 10;
    private static final int MAP_SIZE = 10000;

    public static void main(String[] args) throws ClassNotFoundException, IllegalAccessException, InstantiationException, NoSuchMethodException, InvocationTargetException, MalformedURLException, MalformedObjectNameException, NotCompliantMBeanException, InstanceAlreadyExistsException, MBeanRegistrationException, InterruptedException {
        MBeanServer mbs = ManagementFactory.getPlatformMBeanServer();


        SystemConfig mBean = new SystemConfig(THREADS_COUNT, MAP_SIZE);
        ObjectName name = new ObjectName("com.company.lab10:type=SystemConfig");
        mbs.registerMBean(mBean, name);

        ArrayList<Class> algorithms = new ArrayList<Class>();

        // loading algorithms
        final URL algorithmsFolder = new File("cw1/").toURI().toURL();
        final File folder = new File("cw1/Algorithms/");

        for (final File fileEntry : folder.listFiles()) {
            String className = fileEntry.getName().replace(".class", "");
            Class algorithm = new URLClassLoader(new URL[]{algorithmsFolder}).loadClass("Algorithms." + className);

            for(Method method : algorithm.getMethods())
            {
                if(method.getName().equals("findSolution"))
                {
                    algorithms.add(algorithm);
                }
            }
        }

        SolutionMap<Long, SolutionMapElement> map = new SolutionMap<>(MAP_SIZE);
        ArrayList<SolutionThread> threads = new ArrayList<>();

        for(int i = 0; i < THREADS_COUNT; i++) {
            threads.add(new SolutionThread(i, map, algorithms));
            threads.get(i).start();
        }

        int oldThreadsCount = THREADS_COUNT,
                oldMapSize = MAP_SIZE;

        do {
            Thread.sleep(1000);

            if(oldMapSize != mBean.getMapSize()) {
                map.changeMapSize(mBean.getMapSize());
                map.resetAppeals();
            }

            int threadsCountDiff = oldThreadsCount - mBean.getThreadCount();

            if(threadsCountDiff > 0) {
                for(int i = threadsCountDiff; i > 0; i--) {
                    threads.get(i).kill();
                    threads.remove(i);
                }
            } else if(threadsCountDiff < 0) {
                int count = threads.size();

                for(int i = count; i < Math.abs(threadsCountDiff) + count; i++) {
                    threads.add(new SolutionThread(i, map, algorithms));
                    threads.get(i).start();
                }
            }

            oldMapSize = mBean.getMapSize();
            oldThreadsCount = mBean.getThreadCount();

            mBean.mapRecords = map.size();
            mBean.ratio = (Math.round(map.ratio() * 100.0) / 100.0);

            System.out.println("Wątki: " + threads.size() + " | Rozmiar " + map.size() + "/" + mBean.getMapSize() + " " + (Math.round(map.ratio() * 100.0) / 100.0) + "%");
        } while(mBean.getThreadCount() != 0 );
    }
}
